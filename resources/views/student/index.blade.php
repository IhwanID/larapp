@extends('layouts.app')

@section('content')
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">NIS</th>
                <th scope="col">NAMA</th>
                <th scope="col">ALAMAT</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($students as $i => $student)
            <tr>
            <th scope="row">{{ $i+1 }}</th>
                <td> {{ $student->nis }}</td>
                <td>{{ $student->nama }}</td>
                <td>{{ $student->alamat }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection