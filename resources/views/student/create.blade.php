@extends('layouts.app')

@section('content')
<div class="container">

    <form action="/siswa" method="POST">
        {{ csrf_field() }}   
        <div class="form-group">
          <label for="nis"> NIS</label>
          <input type="text" name="nis" placeholder="masukan nis" class="form-control">
        </div>

        <div class="form-group">
            <label for="nama"> nama</label>
            <input type="text" name="nama" placeholder="masukan nama" class="form-control">
        </div>
       
        <div class="form-group">
            <label for="alamat"> alamat</label>
            <input type="text" name="alamat" placeholder="masukan alamat" class="form-control">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
</div>
@endsection